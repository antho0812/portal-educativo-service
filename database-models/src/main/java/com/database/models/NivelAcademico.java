package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the nivel_academico database table.
 * 
 */
@Entity
@Table(name="nivel_academico")
@NamedQuery(name="NivelAcademico.findAll", query="SELECT n FROM NivelAcademico n")
public class NivelAcademico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descripcion;

	//bi-directional many-to-one association to Nivel
	@ManyToOne
	@JoinColumn(name="id_nivel")
	private Nivel nivel;

	//bi-directional many-to-one association to SeccionNivelAcademico
	@OneToMany(mappedBy="nivelAcademico")
	private List<SeccionNivelAcademico> seccionNivelAcademicos;

	public NivelAcademico() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Nivel getNivel() {
		return this.nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public List<SeccionNivelAcademico> getSeccionNivelAcademicos() {
		return this.seccionNivelAcademicos;
	}

	public void setSeccionNivelAcademicos(List<SeccionNivelAcademico> seccionNivelAcademicos) {
		this.seccionNivelAcademicos = seccionNivelAcademicos;
	}

	public SeccionNivelAcademico addSeccionNivelAcademico(SeccionNivelAcademico seccionNivelAcademico) {
		getSeccionNivelAcademicos().add(seccionNivelAcademico);
		seccionNivelAcademico.setNivelAcademico(this);

		return seccionNivelAcademico;
	}

	public SeccionNivelAcademico removeSeccionNivelAcademico(SeccionNivelAcademico seccionNivelAcademico) {
		getSeccionNivelAcademicos().remove(seccionNivelAcademico);
		seccionNivelAcademico.setNivelAcademico(null);

		return seccionNivelAcademico;
	}

}