package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the alumno_maestro_asignatura_seccion_nivel_academico database table.
 * 
 */
@Entity
@Table(name="alumno_maestro_asignatura_seccion_nivel_academico")
@NamedQuery(name="AlumnoMaestroAsignaturaSeccionNivelAcademico.findAll", query="SELECT a FROM AlumnoMaestroAsignaturaSeccionNivelAcademico a")
public class AlumnoMaestroAsignaturaSeccionNivelAcademico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Alumno
	@ManyToOne
	@JoinColumn(name="id_alumno")
	private Alumno alumno;

	//bi-directional many-to-one association to MaestroAsignaturaSeccionOpcion
	@ManyToOne
	@JoinColumn(name="id_maestro_asignatura_seccion_opcion")
	private MaestroAsignaturaSeccionOpcion maestroAsignaturaSeccionOpcion;

	//bi-directional many-to-one association to Nota
	@OneToMany(mappedBy="alumnoMaestroAsignaturaSeccionNivelAcademico")
	private List<Nota> notas;

	public AlumnoMaestroAsignaturaSeccionNivelAcademico() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Alumno getAlumno() {
		return this.alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public MaestroAsignaturaSeccionOpcion getMaestroAsignaturaSeccionOpcion() {
		return this.maestroAsignaturaSeccionOpcion;
	}

	public void setMaestroAsignaturaSeccionOpcion(MaestroAsignaturaSeccionOpcion maestroAsignaturaSeccionOpcion) {
		this.maestroAsignaturaSeccionOpcion = maestroAsignaturaSeccionOpcion;
	}

	public List<Nota> getNotas() {
		return this.notas;
	}

	public void setNotas(List<Nota> notas) {
		this.notas = notas;
	}

	public Nota addNota(Nota nota) {
		getNotas().add(nota);
		nota.setAlumnoMaestroAsignaturaSeccionNivelAcademico(this);

		return nota;
	}

	public Nota removeNota(Nota nota) {
		getNotas().remove(nota);
		nota.setAlumnoMaestroAsignaturaSeccionNivelAcademico(null);

		return nota;
	}

}