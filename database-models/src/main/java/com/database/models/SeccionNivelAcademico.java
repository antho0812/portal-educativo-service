package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the seccion_nivel_academico database table.
 * 
 */
@Entity
@Table(name="seccion_nivel_academico")
@NamedQuery(name="SeccionNivelAcademico.findAll", query="SELECT s FROM SeccionNivelAcademico s")
public class SeccionNivelAcademico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to MaestroAsignaturaSeccionOpcion
	@OneToMany(mappedBy="seccionNivelAcademico")
	private List<MaestroAsignaturaSeccionOpcion> maestroAsignaturaSeccionOpcions;

	//bi-directional many-to-one association to NivelAcademico
	@ManyToOne
	@JoinColumn(name="id_nivel_academico")
	private NivelAcademico nivelAcademico;

	//bi-directional many-to-one association to Seccion
	@ManyToOne
	@JoinColumn(name="id_seccion")
	private Seccion seccion;

	public SeccionNivelAcademico() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<MaestroAsignaturaSeccionOpcion> getMaestroAsignaturaSeccionOpcions() {
		return this.maestroAsignaturaSeccionOpcions;
	}

	public void setMaestroAsignaturaSeccionOpcions(List<MaestroAsignaturaSeccionOpcion> maestroAsignaturaSeccionOpcions) {
		this.maestroAsignaturaSeccionOpcions = maestroAsignaturaSeccionOpcions;
	}

	public MaestroAsignaturaSeccionOpcion addMaestroAsignaturaSeccionOpcion(MaestroAsignaturaSeccionOpcion maestroAsignaturaSeccionOpcion) {
		getMaestroAsignaturaSeccionOpcions().add(maestroAsignaturaSeccionOpcion);
		maestroAsignaturaSeccionOpcion.setSeccionNivelAcademico(this);

		return maestroAsignaturaSeccionOpcion;
	}

	public MaestroAsignaturaSeccionOpcion removeMaestroAsignaturaSeccionOpcion(MaestroAsignaturaSeccionOpcion maestroAsignaturaSeccionOpcion) {
		getMaestroAsignaturaSeccionOpcions().remove(maestroAsignaturaSeccionOpcion);
		maestroAsignaturaSeccionOpcion.setSeccionNivelAcademico(null);

		return maestroAsignaturaSeccionOpcion;
	}

	public NivelAcademico getNivelAcademico() {
		return this.nivelAcademico;
	}

	public void setNivelAcademico(NivelAcademico nivelAcademico) {
		this.nivelAcademico = nivelAcademico;
	}

	public Seccion getSeccion() {
		return this.seccion;
	}

	public void setSeccion(Seccion seccion) {
		this.seccion = seccion;
	}

}