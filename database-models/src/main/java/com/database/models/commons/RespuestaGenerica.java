package com.database.models.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RespuestaGenerica<T> implements Serializable {

	private static final long serialVersionUID = 5260345576714036090L;
	
	private boolean hasError;
	private T data;
	private List<String> errorMessages = new ArrayList<>();
	

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public boolean isHasError() {
		return hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public void addMessage(String mensaje) {
		errorMessages.add(mensaje);
	}
	
}
