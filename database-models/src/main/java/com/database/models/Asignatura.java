package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the asignatura database table.
 * 
 */
@Entity
@NamedQuery(name="Asignatura.findAll", query="SELECT a FROM Asignatura a")
public class Asignatura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private int estado;

	private String descripcion;

	//bi-directional many-to-one association to MaestroAsignatura
	@OneToMany(mappedBy="asignatura")
	private List<MaestroAsignatura> maestroAsignaturas;

	public Asignatura() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public List<MaestroAsignatura> getMaestroAsignaturas() {
		return this.maestroAsignaturas;
	}

	public void setMaestroAsignaturas(List<MaestroAsignatura> maestroAsignaturas) {
		this.maestroAsignaturas = maestroAsignaturas;
	}

	public MaestroAsignatura addMaestroAsignatura(MaestroAsignatura maestroAsignatura) {
		getMaestroAsignaturas().add(maestroAsignatura);
		maestroAsignatura.setAsignatura(this);

		return maestroAsignatura;
	}

	public MaestroAsignatura removeMaestroAsignatura(MaestroAsignatura maestroAsignatura) {
		getMaestroAsignaturas().remove(maestroAsignatura);
		maestroAsignatura.setAsignatura(null);

		return maestroAsignatura;
	}

}