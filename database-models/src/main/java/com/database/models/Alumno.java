package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the alumno database table.
 * 
 */
@Entity
@NamedQuery(name="Alumno.findAll", query="SELECT a FROM Alumno a")
public class Alumno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name="anio_egreso")
	private Date anioEgreso;

	@Temporal(TemporalType.DATE)
	@Column(name="anio_ingreso")
	private Date anioIngreso;

	@Column(name="nombres_madre")
	private String nombresMadre;

	@Column(name="nombres_padre")
	private String nombresPadre;

	@Column(name="telefono_responsable")
	private String telefonoResponsable;

	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario = new Usuario();

	public Alumno() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getAnioEgreso() {
		return this.anioEgreso;
	}

	public void setAnioEgreso(Date anioEgreso) {
		this.anioEgreso = anioEgreso;
	}

	public Date getAnioIngreso() {
		return this.anioIngreso;
	}

	public void setAnioIngreso(Date anioIngreso) {
		this.anioIngreso = anioIngreso;
	}

	public String getNombresMadre() {
		return this.nombresMadre;
	}

	public void setNombresMadre(String nombresMadre) {
		this.nombresMadre = nombresMadre;
	}

	public String getNombresPadre() {
		return this.nombresPadre;
	}

	public void setNombresPadre(String nombresPadre) {
		this.nombresPadre = nombresPadre;
	}

	public String getTelefonoResponsable() {
		return this.telefonoResponsable;
	}

	public void setTelefonoResponsable(String telefonoResponsable) {
		this.telefonoResponsable = telefonoResponsable;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}