package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the maestro_asignatura database table.
 * 
 */
@Entity
@Table(name="maestro_asignatura")
@NamedQuery(name="MaestroAsignatura.findAll", query="SELECT m FROM MaestroAsignatura m")
public class MaestroAsignatura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Asignatura
	@ManyToOne
	@JoinColumn(name="id_asignatura")
	private Asignatura asignatura;

	//bi-directional many-to-one association to Maestro
	@ManyToOne
	@JoinColumn(name="id_maestro")
	private Maestro maestro;

	//bi-directional many-to-one association to MaestroAsignaturaSeccionOpcion
	@OneToMany(mappedBy="maestroAsignatura")
	private List<MaestroAsignaturaSeccionOpcion> maestroAsignaturaSeccionOpcions;

	public MaestroAsignatura() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Asignatura getAsignatura() {
		return this.asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public Maestro getMaestro() {
		return this.maestro;
	}

	public void setMaestro(Maestro maestro) {
		this.maestro = maestro;
	}

	public List<MaestroAsignaturaSeccionOpcion> getMaestroAsignaturaSeccionOpcions() {
		return this.maestroAsignaturaSeccionOpcions;
	}

	public void setMaestroAsignaturaSeccionOpcions(List<MaestroAsignaturaSeccionOpcion> maestroAsignaturaSeccionOpcions) {
		this.maestroAsignaturaSeccionOpcions = maestroAsignaturaSeccionOpcions;
	}

	public MaestroAsignaturaSeccionOpcion addMaestroAsignaturaSeccionOpcion(MaestroAsignaturaSeccionOpcion maestroAsignaturaSeccionOpcion) {
		getMaestroAsignaturaSeccionOpcions().add(maestroAsignaturaSeccionOpcion);
		maestroAsignaturaSeccionOpcion.setMaestroAsignatura(this);

		return maestroAsignaturaSeccionOpcion;
	}

	public MaestroAsignaturaSeccionOpcion removeMaestroAsignaturaSeccionOpcion(MaestroAsignaturaSeccionOpcion maestroAsignaturaSeccionOpcion) {
		getMaestroAsignaturaSeccionOpcions().remove(maestroAsignaturaSeccionOpcion);
		maestroAsignaturaSeccionOpcion.setMaestroAsignatura(null);

		return maestroAsignaturaSeccionOpcion;
	}

}