package com.database.models;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the notas database table.
 * 
 */
@Entity
@Table(name="notas")
@NamedQuery(name="Nota.findAll", query="SELECT n FROM Nota n")
public class Nota implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private float nota;

	//bi-directional many-to-one association to AlumnoMaestroAsignaturaSeccionNivelAcademico
	@ManyToOne
	@JoinColumn(name="id_alumno_maestro_asignatura_seccion_nivel_academico")
	private AlumnoMaestroAsignaturaSeccionNivelAcademico alumnoMaestroAsignaturaSeccionNivelAcademico;

	//bi-directional many-to-one association to Evaluacion
	@ManyToOne
	@JoinColumn(name="id_evaluacion")
	private Evaluacion evaluacion;

	public Nota() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getNota() {
		return this.nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public AlumnoMaestroAsignaturaSeccionNivelAcademico getAlumnoMaestroAsignaturaSeccionNivelAcademico() {
		return this.alumnoMaestroAsignaturaSeccionNivelAcademico;
	}

	public void setAlumnoMaestroAsignaturaSeccionNivelAcademico(AlumnoMaestroAsignaturaSeccionNivelAcademico alumnoMaestroAsignaturaSeccionNivelAcademico) {
		this.alumnoMaestroAsignaturaSeccionNivelAcademico = alumnoMaestroAsignaturaSeccionNivelAcademico;
	}

	public Evaluacion getEvaluacion() {
		return this.evaluacion;
	}

	public void setEvaluacion(Evaluacion evaluacion) {
		this.evaluacion = evaluacion;
	}

}