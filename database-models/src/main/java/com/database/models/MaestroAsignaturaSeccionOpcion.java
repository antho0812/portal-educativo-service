package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the maestro_asignatura_seccion_opcion database table.
 * 
 */
@Entity
@Table(name="maestro_asignatura_seccion_opcion")
@NamedQuery(name="MaestroAsignaturaSeccionOpcion.findAll", query="SELECT m FROM MaestroAsignaturaSeccionOpcion m")
public class MaestroAsignaturaSeccionOpcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to AlumnoMaestroAsignaturaSeccionNivelAcademico
	@OneToMany(mappedBy="maestroAsignaturaSeccionOpcion")
	private List<AlumnoMaestroAsignaturaSeccionNivelAcademico> alumnoMaestroAsignaturaSeccionNivelAcademicos;

	//bi-directional many-to-one association to MaestroAsignatura
	@ManyToOne
	@JoinColumn(name="id_maestro_asignatura")
	private MaestroAsignatura maestroAsignatura;

	//bi-directional many-to-one association to SeccionNivelAcademico
	@ManyToOne
	@JoinColumn(name="id_seccion_nivel_academico")
	private SeccionNivelAcademico seccionNivelAcademico;

	public MaestroAsignaturaSeccionOpcion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<AlumnoMaestroAsignaturaSeccionNivelAcademico> getAlumnoMaestroAsignaturaSeccionNivelAcademicos() {
		return this.alumnoMaestroAsignaturaSeccionNivelAcademicos;
	}

	public void setAlumnoMaestroAsignaturaSeccionNivelAcademicos(List<AlumnoMaestroAsignaturaSeccionNivelAcademico> alumnoMaestroAsignaturaSeccionNivelAcademicos) {
		this.alumnoMaestroAsignaturaSeccionNivelAcademicos = alumnoMaestroAsignaturaSeccionNivelAcademicos;
	}

	public AlumnoMaestroAsignaturaSeccionNivelAcademico addAlumnoMaestroAsignaturaSeccionNivelAcademico(AlumnoMaestroAsignaturaSeccionNivelAcademico alumnoMaestroAsignaturaSeccionNivelAcademico) {
		getAlumnoMaestroAsignaturaSeccionNivelAcademicos().add(alumnoMaestroAsignaturaSeccionNivelAcademico);
		alumnoMaestroAsignaturaSeccionNivelAcademico.setMaestroAsignaturaSeccionOpcion(this);

		return alumnoMaestroAsignaturaSeccionNivelAcademico;
	}

	public AlumnoMaestroAsignaturaSeccionNivelAcademico removeAlumnoMaestroAsignaturaSeccionNivelAcademico(AlumnoMaestroAsignaturaSeccionNivelAcademico alumnoMaestroAsignaturaSeccionNivelAcademico) {
		getAlumnoMaestroAsignaturaSeccionNivelAcademicos().remove(alumnoMaestroAsignaturaSeccionNivelAcademico);
		alumnoMaestroAsignaturaSeccionNivelAcademico.setMaestroAsignaturaSeccionOpcion(null);

		return alumnoMaestroAsignaturaSeccionNivelAcademico;
	}

	public MaestroAsignatura getMaestroAsignatura() {
		return this.maestroAsignatura;
	}

	public void setMaestroAsignatura(MaestroAsignatura maestroAsignatura) {
		this.maestroAsignatura = maestroAsignatura;
	}

	public SeccionNivelAcademico getSeccionNivelAcademico() {
		return this.seccionNivelAcademico;
	}

	public void setSeccionNivelAcademico(SeccionNivelAcademico seccionNivelAcademico) {
		this.seccionNivelAcademico = seccionNivelAcademico;
	}

}