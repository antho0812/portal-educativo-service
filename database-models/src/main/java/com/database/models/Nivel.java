package com.database.models;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the nivel database table.
 * 
 */
@Entity
@NamedQuery(name="Nivel.findAll", query="SELECT n FROM Nivel n")
public class Nivel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descripcion;

	//bi-directional many-to-one association to NivelAcademico
	@OneToMany(mappedBy="nivel")
	private List<NivelAcademico> nivelAcademicos;

	public Nivel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<NivelAcademico> getNivelAcademicos() {
		return this.nivelAcademicos;
	}

	public void setNivelAcademicos(List<NivelAcademico> nivelAcademicos) {
		this.nivelAcademicos = nivelAcademicos;
	}

	public NivelAcademico addNivelAcademico(NivelAcademico nivelAcademico) {
		getNivelAcademicos().add(nivelAcademico);
		nivelAcademico.setNivel(this);

		return nivelAcademico;
	}

	public NivelAcademico removeNivelAcademico(NivelAcademico nivelAcademico) {
		getNivelAcademicos().remove(nivelAcademico);
		nivelAcademico.setNivel(null);

		return nivelAcademico;
	}

}