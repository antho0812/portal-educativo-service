package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Seccion;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.SeccionService;

@RestController
@RequestMapping(value="/seccion")
public class SeccionController {
	@Autowired
	private SeccionService seccionService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Seccion>> insertarSeccion(@RequestBody Seccion seccion) {
		RespuestaGenerica<Seccion> respuesta = new RespuestaGenerica<>();
		try {
			seccion = seccionService.save(seccion);
			respuesta.setHasError(false);
			respuesta.setData(seccion);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Seccion>> actualizarSeccion(@RequestBody Seccion seccion) {
		RespuestaGenerica<Seccion> respuesta = new RespuestaGenerica<>();
		try {
			seccion = seccionService.save(seccion);
			respuesta.setHasError(false);
			respuesta.setData(seccion);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Seccion>>> obtenerSeccions() {
		RespuestaGenerica<List<Seccion>> respuesta = new RespuestaGenerica<>();
		try {
			List<Seccion> seccions = seccionService.obtenerSeccions();
			respuesta.setHasError(false);
			respuesta.setData(seccions);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Seccion>> obtenerSeccion(@PathVariable Integer id) {
		RespuestaGenerica<Seccion> respuesta = new RespuestaGenerica<>();
		try {

			Seccion seccion = seccionService.obtenerSeccion(id);

			if (seccion == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Seccion no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(seccion);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
