package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Nivel;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.NivelService;

@RestController
@RequestMapping(value="/nivel")
public class NivelController {
	@Autowired
	private NivelService nivelService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Nivel>> insertarNivel(@RequestBody Nivel nivel) {
		RespuestaGenerica<Nivel> respuesta = new RespuestaGenerica<>();
		try {
			nivel = nivelService.save(nivel);
			respuesta.setHasError(false);
			respuesta.setData(nivel);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Nivel>> actualizarNivel(@RequestBody Nivel nivel) {
		RespuestaGenerica<Nivel> respuesta = new RespuestaGenerica<>();
		try {
			nivel = nivelService.save(nivel);
			respuesta.setHasError(false);
			respuesta.setData(nivel);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Nivel>>> obtenerNivels() {
		RespuestaGenerica<List<Nivel>> respuesta = new RespuestaGenerica<>();
		try {
			List<Nivel> nivels = nivelService.obtenerNivels();
			respuesta.setHasError(false);
			respuesta.setData(nivels);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Nivel>> obtenerNivel(@PathVariable Integer id) {
		RespuestaGenerica<Nivel> respuesta = new RespuestaGenerica<>();
		try {

			Nivel nivel = nivelService.obtenerNivel(id);

			if (nivel == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Nivel no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(nivel);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
