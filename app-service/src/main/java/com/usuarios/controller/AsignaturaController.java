package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Asignatura;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.AsignaturaService;

@RestController
@RequestMapping(value="/asignatura")
public class AsignaturaController {
	@Autowired
	private AsignaturaService asignaturaService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Asignatura>> insertarAsignatura(@RequestBody Asignatura asignatura) {
		RespuestaGenerica<Asignatura> respuesta = new RespuestaGenerica<>();
		try {
			asignatura = asignaturaService.save(asignatura);
			respuesta.setHasError(false);
			respuesta.setData(asignatura);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Asignatura>> actualizarAsignatura(@RequestBody Asignatura asignatura) {
		RespuestaGenerica<Asignatura> respuesta = new RespuestaGenerica<>();
		try {
			asignatura = asignaturaService.save(asignatura);
			respuesta.setHasError(false);
			respuesta.setData(asignatura);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Asignatura>>> obtenerAsignaturas() {
		RespuestaGenerica<List<Asignatura>> respuesta = new RespuestaGenerica<>();
		try {
			List<Asignatura> asignaturas = asignaturaService.obtenerAsignaturas();
			respuesta.setHasError(false);
			respuesta.setData(asignaturas);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Asignatura>> obtenerAsignatura(@PathVariable Integer id) {
		RespuestaGenerica<Asignatura> respuesta = new RespuestaGenerica<>();
		try {

			Asignatura asignatura = asignaturaService.obtenerAsignatura(id);

			if (asignatura == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Asignatura no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(asignatura);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
