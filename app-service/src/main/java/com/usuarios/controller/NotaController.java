package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Nota;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.NotaService;

@RestController
@RequestMapping(value="/nota")
public class NotaController {
	@Autowired
	private NotaService notaService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Nota>> insertarNota(@RequestBody Nota nota) {
		RespuestaGenerica<Nota> respuesta = new RespuestaGenerica<>();
		try {
			nota = notaService.save(nota);
			respuesta.setHasError(false);
			respuesta.setData(nota);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Nota>> actualizarNota(@RequestBody Nota nota) {
		RespuestaGenerica<Nota> respuesta = new RespuestaGenerica<>();
		try {
			nota = notaService.save(nota);
			respuesta.setHasError(false);
			respuesta.setData(nota);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Nota>>> obtenerNotas() {
		RespuestaGenerica<List<Nota>> respuesta = new RespuestaGenerica<>();
		try {
			List<Nota> notas = notaService.obtenerNotas();
			respuesta.setHasError(false);
			respuesta.setData(notas);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Nota>> obtenerNota(@PathVariable Integer id) {
		RespuestaGenerica<Nota> respuesta = new RespuestaGenerica<>();
		try {

			Nota nota = notaService.obtenerNota(id);

			if (nota == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Nota no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(nota);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
