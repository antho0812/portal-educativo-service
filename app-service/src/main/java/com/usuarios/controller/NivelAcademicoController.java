package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.NivelAcademico;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.NivelAcademicoService;

@RestController
@RequestMapping(value="/nivelacademico")
public class NivelAcademicoController {
	@Autowired
	private NivelAcademicoService nivelAcademicoService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<NivelAcademico>> insertarNivelAcademico(@RequestBody NivelAcademico nivelAcademico) {
		RespuestaGenerica<NivelAcademico> respuesta = new RespuestaGenerica<>();
		try {
			nivelAcademico = nivelAcademicoService.save(nivelAcademico);
			respuesta.setHasError(false);
			respuesta.setData(nivelAcademico);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<NivelAcademico>> actualizarNivelAcademico(@RequestBody NivelAcademico nivelAcademico) {
		RespuestaGenerica<NivelAcademico> respuesta = new RespuestaGenerica<>();
		try {
			nivelAcademico = nivelAcademicoService.save(nivelAcademico);
			respuesta.setHasError(false);
			respuesta.setData(nivelAcademico);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<NivelAcademico>>> obtenerNivelAcademicos() {
		RespuestaGenerica<List<NivelAcademico>> respuesta = new RespuestaGenerica<>();
		try {
			List<NivelAcademico> nivelAcademicos = nivelAcademicoService.obtenerNivelAcademicos();
			respuesta.setHasError(false);
			respuesta.setData(nivelAcademicos);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<NivelAcademico>> obtenerNivelAcademico(@PathVariable Integer id) {
		RespuestaGenerica<NivelAcademico> respuesta = new RespuestaGenerica<>();
		try {

			NivelAcademico nivelAcademico = nivelAcademicoService.obtenerNivelAcademico(id);

			if (nivelAcademico == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("NivelAcademico no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(nivelAcademico);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
