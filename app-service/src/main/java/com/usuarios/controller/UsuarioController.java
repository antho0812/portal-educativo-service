package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Usuario;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.UsuarioService;

@RestController
@RequestMapping(value="/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Usuario>> insertarUsuario(@RequestBody Usuario usuario) {
		RespuestaGenerica<Usuario> respuesta = new RespuestaGenerica<>();
		try {
			usuario = usuarioService.save(usuario);
			respuesta.setHasError(false);
			respuesta.setData(usuario);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Usuario>> actualizarUsuario(@RequestBody Usuario usuario) {
		RespuestaGenerica<Usuario> respuesta = new RespuestaGenerica<>();
		try {
			usuario = usuarioService.save(usuario);
			respuesta.setHasError(false);
			respuesta.setData(usuario);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Usuario>>> obtenerUsuarios() {
		RespuestaGenerica<List<Usuario>> respuesta = new RespuestaGenerica<>();
		try {
			List<Usuario> usuarios =  usuarioService.obtenerUsuarios();
			respuesta.setHasError(false);
			respuesta.setData(usuarios);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Usuario>> obtenerUsuario(@PathVariable Integer id) {
		RespuestaGenerica<Usuario> respuesta = new RespuestaGenerica<>();
		try {
			
			Usuario usuario =  usuarioService.obtenerUsuario(id);
			
			if(usuario == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Usuario no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}
			
			respuesta.setHasError(false);
			respuesta.setData(usuario);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);


		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
