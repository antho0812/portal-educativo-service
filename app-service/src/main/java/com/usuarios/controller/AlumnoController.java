package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Alumno;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.AlumnoService;

@RestController
@RequestMapping(value="/alumno")
public class AlumnoController {
	@Autowired
	private AlumnoService alumnoService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Alumno>> insertarAlumno(@RequestBody Alumno alumno) {
		RespuestaGenerica<Alumno> respuesta = new RespuestaGenerica<>();
		try {
			alumno = alumnoService.save(alumno);
			respuesta.setHasError(false);
			respuesta.setData(alumno);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Alumno>> actualizarAlumno(@RequestBody Alumno alumno) {
		RespuestaGenerica<Alumno> respuesta = new RespuestaGenerica<>();
		try {
			alumno = alumnoService.save(alumno);
			respuesta.setHasError(false);
			respuesta.setData(alumno);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Alumno>>> obtenerAlumnos() {
		RespuestaGenerica<List<Alumno>> respuesta = new RespuestaGenerica<>();
		try {
			List<Alumno> alumnos = alumnoService.obtenerAlumnos();
			respuesta.setHasError(false);
			respuesta.setData(alumnos);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Alumno>> obtenerAlumno(@PathVariable Integer id) {
		RespuestaGenerica<Alumno> respuesta = new RespuestaGenerica<>();
		try {

			Alumno alumno = alumnoService.obtenerAlumno(id);

			if (alumno == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Alumno no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(alumno);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
