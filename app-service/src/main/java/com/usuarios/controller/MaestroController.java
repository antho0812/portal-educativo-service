package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Maestro;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.MaestroService;

@RestController
@RequestMapping(value="/maestro")
public class MaestroController {
	@Autowired
	private MaestroService maestroService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Maestro>> insertarMaestro(@RequestBody Maestro maestro) {
		RespuestaGenerica<Maestro> respuesta = new RespuestaGenerica<>();
		try {
			maestroService.save(maestro, respuesta);
			
			if(respuesta.isHasError()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(respuesta);
			}
			
			respuesta.getData().getUsuario().setPassword("");
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Maestro>> actualizarMaestro(@RequestBody Maestro maestro) {
		RespuestaGenerica<Maestro> respuesta = new RespuestaGenerica<>();
		try {
			maestroService.save(maestro, respuesta);
			
			if(respuesta.isHasError()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(respuesta);
			}
			
			respuesta.getData().getUsuario().setPassword("");
			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Maestro>>> obtenerMaestros() {
		RespuestaGenerica<List<Maestro>> respuesta = new RespuestaGenerica<>();
		try {
			List<Maestro> maestros = maestroService.obtenerMaestros();
			respuesta.setHasError(false);
			respuesta.setData(maestros);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Maestro>> obtenerMaestro(@PathVariable Integer id) {
		RespuestaGenerica<Maestro> respuesta = new RespuestaGenerica<>();
		try {

			Maestro maestro = maestroService.obtenerMaestro(id);

			if (maestro == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Maestro no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(maestro);
			return ResponseEntity.status(HttpStatus.OK).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
