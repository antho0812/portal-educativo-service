package com.usuarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.database.models.Evaluacion;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.service.EvaluacionService;

@RestController
@RequestMapping(value="/evaluacion")
public class EvaluacionController {
	@Autowired
	private EvaluacionService evaluacionService;

	@PostMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Evaluacion>> insertarEvaluacion(@RequestBody Evaluacion evaluacion) {
		RespuestaGenerica<Evaluacion> respuesta = new RespuestaGenerica<>();
		try {
			evaluacion = evaluacionService.save(evaluacion);
			respuesta.setHasError(false);
			respuesta.setData(evaluacion);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@PutMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<Evaluacion>> actualizarEvaluacion(@RequestBody Evaluacion evaluacion) {
		RespuestaGenerica<Evaluacion> respuesta = new RespuestaGenerica<>();
		try {
			evaluacion = evaluacionService.save(evaluacion);
			respuesta.setHasError(false);
			respuesta.setData(evaluacion);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/")
	public ResponseEntity<RespuestaGenerica<List<Evaluacion>>> obtenerEvaluacions() {
		RespuestaGenerica<List<Evaluacion>> respuesta = new RespuestaGenerica<>();
		try {
			List<Evaluacion> evaluacions = evaluacionService.obtenerEvaluacions();
			respuesta.setHasError(false);
			respuesta.setData(evaluacions);

			return ResponseEntity.ok().body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<RespuestaGenerica<Evaluacion>> obtenerEvaluacion(@PathVariable Integer id) {
		RespuestaGenerica<Evaluacion> respuesta = new RespuestaGenerica<>();
		try {

			Evaluacion evaluacion = evaluacionService.obtenerEvaluacion(id);

			if (evaluacion == null) {
				respuesta.setHasError(true);
				respuesta.addMessage("Evaluacion no encontrado");
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);
			}

			respuesta.setHasError(false);
			respuesta.setData(evaluacion);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(respuesta);

		} catch (Exception e) {
			respuesta.setHasError(true);
			respuesta.addMessage(e.getMessage());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(respuesta);
		}
	}

}
