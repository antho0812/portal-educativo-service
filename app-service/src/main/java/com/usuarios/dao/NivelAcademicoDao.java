package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.NivelAcademico;

@Repository
public interface NivelAcademicoDao extends JpaRepository<NivelAcademico, Integer>{
	
}
