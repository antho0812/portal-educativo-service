package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.database.models.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Integer>{
	
	@Query("select u from Usuario u where usuario = ?1")
	public Usuario buscarByUsuario(String usuario);
	
}
