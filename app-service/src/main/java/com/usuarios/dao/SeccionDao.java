package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Seccion;

@Repository
public interface SeccionDao extends JpaRepository<Seccion, Integer>{
	
}
