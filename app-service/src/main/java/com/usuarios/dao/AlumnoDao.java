package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Alumno;

@Repository
public interface AlumnoDao extends JpaRepository<Alumno, Integer>{
	
}
