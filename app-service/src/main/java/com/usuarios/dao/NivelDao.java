package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Nivel;

@Repository
public interface NivelDao extends JpaRepository<Nivel, Integer>{
	
}
