package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Maestro;

@Repository
public interface MaestroDao extends JpaRepository<Maestro, Integer>{
	
}
