package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Evaluacion;

@Repository
public interface EvaluacionDao extends JpaRepository<Evaluacion, Integer>{
	
}
