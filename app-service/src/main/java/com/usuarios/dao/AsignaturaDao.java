package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Asignatura;

@Repository
public interface AsignaturaDao extends JpaRepository<Asignatura, Integer>{
	
}
