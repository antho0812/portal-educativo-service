package com.usuarios.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.database.models.Nota;

@Repository
public interface NotaDao extends JpaRepository<Nota, Integer>{
	
}
