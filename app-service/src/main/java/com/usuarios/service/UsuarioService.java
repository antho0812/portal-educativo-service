package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Usuario;
import com.usuarios.dao.UsuarioDao;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioDao dao;
	
	@Transactional
	public Usuario save(Usuario usuario) {
		// TODO Auto-generated method stub
		return dao.save(usuario);
	}
	
	@Transactional(readOnly = true)
	public List<Usuario> obtenerUsuarios() {
		// TODO Auto-generated method stub
		return (List<Usuario>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Usuario obtenerUsuario(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
