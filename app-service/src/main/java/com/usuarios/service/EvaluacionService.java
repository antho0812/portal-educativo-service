package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Evaluacion;
import com.usuarios.dao.EvaluacionDao;

@Service
public class EvaluacionService {
	
	@Autowired
	private EvaluacionDao dao;
	
	@Transactional
	public Evaluacion save(Evaluacion evaluacion) {
		// TODO Auto-generated method stub
		return dao.save(evaluacion);
	}
	
	@Transactional(readOnly = true)
	public List<Evaluacion> obtenerEvaluacions() {
		// TODO Auto-generated method stub
		return (List<Evaluacion>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Evaluacion obtenerEvaluacion(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
