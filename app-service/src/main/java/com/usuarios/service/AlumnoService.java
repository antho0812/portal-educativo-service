package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Alumno;
import com.usuarios.dao.AlumnoDao;

@Service
public class AlumnoService {
	
	@Autowired
	private AlumnoDao dao;
	
	@Transactional
	public Alumno save(Alumno alumno) {
		// TODO Auto-generated method stub
		return dao.save(alumno);
	}
	
	@Transactional(readOnly = true)
	public List<Alumno> obtenerAlumnos() {
		// TODO Auto-generated method stub
		return (List<Alumno>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Alumno obtenerAlumno(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
