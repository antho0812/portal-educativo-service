package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Seccion;
import com.usuarios.dao.SeccionDao;

@Service
public class SeccionService {
	
	@Autowired
	private SeccionDao dao;
	
	@Transactional
	public Seccion save(Seccion seccion) {
		// TODO Auto-generated method stub
		return dao.save(seccion);
	}
	
	@Transactional(readOnly = true)
	public List<Seccion> obtenerSeccions() {
		// TODO Auto-generated method stub
		return (List<Seccion>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Seccion obtenerSeccion(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
