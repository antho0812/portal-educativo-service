package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Nivel;
import com.usuarios.dao.NivelDao;

@Service
public class NivelService {
	
	@Autowired
	private NivelDao dao;
	
	@Transactional
	public Nivel save(Nivel nivel) {
		// TODO Auto-generated method stub
		return dao.save(nivel);
	}
	
	@Transactional(readOnly = true)
	public List<Nivel> obtenerNivels() {
		// TODO Auto-generated method stub
		return (List<Nivel>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Nivel obtenerNivel(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
