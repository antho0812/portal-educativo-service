package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Nota;
import com.usuarios.dao.NotaDao;

@Service
public class NotaService {
	
	@Autowired
	private NotaDao dao;
	
	@Transactional
	public Nota save(Nota nota) {
		// TODO Auto-generated method stub
		return dao.save(nota);
	}
	
	@Transactional(readOnly = true)
	public List<Nota> obtenerNotas() {
		// TODO Auto-generated method stub
		return (List<Nota>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Nota obtenerNota(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
