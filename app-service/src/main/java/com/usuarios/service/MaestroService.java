package com.usuarios.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Maestro;
import com.database.models.Usuario;
import com.database.models.commons.RespuestaGenerica;
import com.usuarios.dao.MaestroDao;
import com.usuarios.dao.UsuarioDao;

@Service
public class MaestroService {
	
	@Autowired
	private BCryptPasswordEncoder passwordEnconder;
	
	@Autowired
	private MaestroDao dao;
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Transactional
	public RespuestaGenerica<Maestro> save(Maestro maestro, RespuestaGenerica<Maestro> rg) {
		// TODO Auto-generated method stub
		try {
			
			Usuario usuario;
			
			if(maestro.getId() == 0) { // si es 0 es porque es un usuario nuevo
				usuario = usuarioDao.buscarByUsuario(maestro.getUsuario().getUsuario());
				
				if(usuario != null) {
					rg.setHasError(true);
					rg.addMessage("El usuario ".concat(maestro.getUsuario().getUsuario().concat(" ya esta registrado")));
					return rg;
				}
			}
			
			usuario = maestro.getUsuario();
			String password = usuario.getUsuario();
			password = password.concat("." + LocalDate.now().getYear() + "");
			
			usuario.setPassword(passwordEnconder.encode(password));
			rg.setData(dao.save(maestro));
			rg.setHasError(false);
			
		} catch (Exception e) {
			// TODO: handle exception
			rg.setHasError(true);
			rg.addMessage(e.getMessage());
		}
		return rg;
	}
	
	@Transactional(readOnly = true)
	public List<Maestro> obtenerMaestros() {
		// TODO Auto-generated method stub
		return (List<Maestro>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Maestro obtenerMaestro(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
