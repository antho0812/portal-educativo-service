package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.Asignatura;
import com.usuarios.dao.AsignaturaDao;

@Service
public class AsignaturaService {
	
	@Autowired
	private AsignaturaDao dao;
	
	@Transactional
	public Asignatura save(Asignatura asignatura) {
		// TODO Auto-generated method stub
		return dao.save(asignatura);
	}
	
	@Transactional(readOnly = true)
	public List<Asignatura> obtenerAsignaturas() {
		// TODO Auto-generated method stub
		return (List<Asignatura>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public Asignatura obtenerAsignatura(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
