package com.usuarios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.database.models.NivelAcademico;
import com.usuarios.dao.NivelAcademicoDao;

@Service
public class NivelAcademicoService {
	
	@Autowired
	private NivelAcademicoDao dao;
	
	@Transactional
	public NivelAcademico save(NivelAcademico nivelAcademico) {
		// TODO Auto-generated method stub
		return dao.save(nivelAcademico);
	}
	
	@Transactional(readOnly = true)
	public List<NivelAcademico> obtenerNivelAcademicos() {
		// TODO Auto-generated method stub
		return (List<NivelAcademico>)dao.findAll();
	}

	@Transactional(readOnly = true)
	public NivelAcademico obtenerNivelAcademico(Integer id) {
		// TODO Auto-generated method stub
		return dao.findById(id).orElse(null);
	}

}
