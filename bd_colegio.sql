-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3308
-- Tiempo de generación: 07-05-2021 a las 21:56:08
-- Versión del servidor: 8.0.18
-- Versión de PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_colegio`
--
CREATE DATABASE IF NOT EXISTS `bd_colegio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bd_colegio`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nombres_padre` varchar(100) NOT NULL,
  `nombres_madre` varchar(100) NOT NULL,
  `telefono_responsable` varchar(45) NOT NULL,
  `anio_ingreso` date DEFAULT NULL,
  `anio_egreso` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario_idx` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_maestro_asignatura_seccion_nivel_academico`
--

DROP TABLE IF EXISTS `alumno_maestro_asignatura_seccion_nivel_academico`;
CREATE TABLE IF NOT EXISTS `alumno_maestro_asignatura_seccion_nivel_academico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_maestro_asignatura_seccion_opcion` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_maestro_asignatura_seccion_opcion_idx` (`id_maestro_asignatura_seccion_opcion`),
  KEY `id_alumno_idx` (`id_alumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
CREATE TABLE IF NOT EXISTS `asignatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion`
--

DROP TABLE IF EXISTS `evaluacion`;
CREATE TABLE IF NOT EXISTS `evaluacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maestro`
--

DROP TABLE IF EXISTS `maestro`;
CREATE TABLE IF NOT EXISTS `maestro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario_idx` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maestro_asignatura`
--

DROP TABLE IF EXISTS `maestro_asignatura`;
CREATE TABLE IF NOT EXISTS `maestro_asignatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_asignatura` int(11) NOT NULL,
  `id_maestro` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_asignatura_idx` (`id_asignatura`),
  KEY `id_maestro_idx` (`id_maestro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maestro_asignatura_seccion_opcion`
--

DROP TABLE IF EXISTS `maestro_asignatura_seccion_opcion`;
CREATE TABLE IF NOT EXISTS `maestro_asignatura_seccion_opcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_maestro_asignatura` int(11) NOT NULL,
  `id_seccion_nivel_academico` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_maestro_asignatura_idx` (`id_maestro_asignatura`),
  KEY `id_seccion_nivel_academico_idx` (`id_seccion_nivel_academico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

DROP TABLE IF EXISTS `nivel`;
CREATE TABLE IF NOT EXISTS `nivel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_academico`
--

DROP TABLE IF EXISTS `nivel_academico`;
CREATE TABLE IF NOT EXISTS `nivel_academico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_nivel` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_nivel_idx` (`id_nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

DROP TABLE IF EXISTS `notas`;
CREATE TABLE IF NOT EXISTS `notas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno_maestro_asignatura_seccion_nivel_academico` int(11) NOT NULL,
  `id_evaluacion` int(11) NOT NULL,
  `nota` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_alumno_maestro_asignatura_seccion_nivel_academico_idx` (`id_alumno_maestro_asignatura_seccion_nivel_academico`),
  KEY `id_evaluacion_idx` (`id_evaluacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

DROP TABLE IF EXISTS `seccion`;
CREATE TABLE IF NOT EXISTS `seccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `anio` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_nivel_academico`
--

DROP TABLE IF EXISTS `seccion_nivel_academico`;
CREATE TABLE IF NOT EXISTS `seccion_nivel_academico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_seccion` int(11) NOT NULL,
  `id_nivel_academico` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_seccion_idx` (`id_seccion`),
  KEY `id_nivel_academico_idx` (`id_nivel_academico`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(60) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `dui` int(11) DEFAULT NULL,
  `nit` int(11) DEFAULT NULL,
  `telefono` int(11) NOT NULL,
  `correo` varchar(120) NOT NULL,
  `direccion` varchar(120) NOT NULL,
  `password` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `id_usuario_a` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `alumno_maestro_asignatura_seccion_nivel_academico`
--
ALTER TABLE `alumno_maestro_asignatura_seccion_nivel_academico`
  ADD CONSTRAINT `id_alumno` FOREIGN KEY (`id_alumno`) REFERENCES `alumno` (`id`),
  ADD CONSTRAINT `id_maestro_asignatura_seccion_opcion` FOREIGN KEY (`id_maestro_asignatura_seccion_opcion`) REFERENCES `maestro_asignatura_seccion_opcion` (`id`);

--
-- Filtros para la tabla `maestro`
--
ALTER TABLE `maestro`
  ADD CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `maestro_asignatura`
--
ALTER TABLE `maestro_asignatura`
  ADD CONSTRAINT `id_asignatura` FOREIGN KEY (`id_asignatura`) REFERENCES `asignatura` (`id`),
  ADD CONSTRAINT `id_maestro` FOREIGN KEY (`id_maestro`) REFERENCES `maestro` (`id`);

--
-- Filtros para la tabla `maestro_asignatura_seccion_opcion`
--
ALTER TABLE `maestro_asignatura_seccion_opcion`
  ADD CONSTRAINT `id_maestro_asignatura` FOREIGN KEY (`id_maestro_asignatura`) REFERENCES `maestro_asignatura` (`id`),
  ADD CONSTRAINT `id_seccion_nivel_academico` FOREIGN KEY (`id_seccion_nivel_academico`) REFERENCES `seccion_nivel_academico` (`id`);

--
-- Filtros para la tabla `nivel_academico`
--
ALTER TABLE `nivel_academico`
  ADD CONSTRAINT `id_nivel` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id`);

--
-- Filtros para la tabla `notas`
--
ALTER TABLE `notas`
  ADD CONSTRAINT `id_alumno_maestro_asignatura_seccion_nivel_academico` FOREIGN KEY (`id_alumno_maestro_asignatura_seccion_nivel_academico`) REFERENCES `alumno_maestro_asignatura_seccion_nivel_academico` (`id`),
  ADD CONSTRAINT `id_evaluacion` FOREIGN KEY (`id_evaluacion`) REFERENCES `evaluacion` (`id`);

--
-- Filtros para la tabla `seccion_nivel_academico`
--
ALTER TABLE `seccion_nivel_academico`
  ADD CONSTRAINT `id_nivel_academico` FOREIGN KEY (`id_nivel_academico`) REFERENCES `nivel_academico` (`id`),
  ADD CONSTRAINT `id_seccion` FOREIGN KEY (`id_seccion`) REFERENCES `seccion` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
